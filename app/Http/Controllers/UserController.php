<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\GetCountryRequest;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

final class UserController extends Controller
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index(GetCountryRequest $request): JsonResponse
    {
        return response()->json(
            $this->userService->getUsersInCountry($request->get('country')),
            Response::HTTP_OK
        );
    }
}
