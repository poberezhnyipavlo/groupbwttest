<?php

namespace App\Http\Requests\User;

use App\Models\Country;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

final class GetCountryRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'country' => [
                'required',
                'string',
                Rule::exists(Country::class, 'name'),
            ],
        ];
    }
}
