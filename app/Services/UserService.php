<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

final class UserService
{
    public function getUsersInCountry(string $countryName): Collection
    {
        return User::whereHas('companies.country', fn(Builder $query) => $query->where('name', $countryName))
            ->with('companies')
            ->get()
        ;
    }
}
